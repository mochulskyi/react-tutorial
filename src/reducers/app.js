import types from "../actions/app/types";

const initialState = {
    isDrawerOpened: false,
    isOverlayShown: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.drawerOpen:
        case types.drawerClose: {
            return {
                ...state,
                isDrawerOpened: action.payload
            }
        }
        case types.overlayShow:
        case types.overlayHide: {
            return {
                ...state,
                isOverlayShown: action.payload
            }
        }
        default: {
            return state
        }
    }
}
