import types from "../actions/cart/types";

const initialState = {
    isDetailsLoading: false,
    cartId: null,
    items: [],
    count: 0,
    qty: 0
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.createCart: {
            return {
                ...state,
                cartId: action.payload
            }
        }
        case types.getCartDetailsRequest: {
            return {
                ...state,
                isDetailsLoading: true
            }
        }
        case types.getCartDetailsReceive: {
            return {
                ...state,
                isDetailsLoading: false,
                items: action.payload.items,
                count: action.payload.items_qty,
                qty: action.payload.items_count
            }
        }
        default: {
            return state
        }
    }
}
