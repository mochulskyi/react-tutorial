const config = {
    "absolute_footer": null,
    "base_currency_code": "PLN",
    "base_link_url": "http://magento23.g4n.eu/",
    "base_media_url": "http://magento23.g4n.eu/pub/media/",
    "base_static_url": "https://magento23.g4n.eu/pub/static/version1563518494/",
    "base_url": "http://magento23.g4n.eu/",
    "cms_home_page": "home",
    "cms_no_cookies": "enable-cookies",
    "cms_no_route": "no-route",
    "code": "default",
    "copyright": "Copyright © 2019-present Magento, Inc. All rights reserved.",
    "default_description": "cokolwiek",
    "default_display_currency_code": "PLN",
    "default_keywords": "asd",
    "default_title": "Magento Commerce",
    "demonotice": 0,
    "front": "cms",
    "head_includes": null,
    "head_shortcut_icon": "stores/1/favicon.ico",
    "header_logo_src": "stores/1/colcci.png",
    "id": 1,
    "locale": "pl_PL",
    "logo_alt": null,
    "logo_height": null,
    "logo_width": null,
    "no_route": "cms/noroute/index",
    "secure_base_link_url": "https://magento23.g4n.eu/",
    "secure_base_media_url": "https://magento23.g4n.eu/pub/media/",
    "secure_base_static_url": "https://magento23.g4n.eu/pub/static/version1563518494/",
    "secure_base_url": "https://magento23.g4n.eu/",
    "show_cms_breadcrumbs": 1,
    "timezone": "Europe/Warsaw",
    "title_prefix": null,
    "title_suffix": null,
    "website_id": 1,
    "weight_unit": "kgs",
    "welcome": "Default welcome msg!"
};
const categories = [
    {
        "available_sort_by": [],
        "children_count": "0",
        "created_at": "2020-02-02 16:58:24",
        "default_sort_by": null,
        "description": "testowy opis dla kategorii bardzo dlugi, testowy opis dla kategorii bardzo dlugi, testowy opis dla kategorii bardzo dlugi, testowy opis dla kategorii bardzo dlugi",
        "display_mode": "PRODUCTS",
        "filter_price_range": null,
        "id": 6,
        "image": null,
        "include_in_menu": 1,
        "is_anchor": 1,
        "landing_page": null,
        "level": 2,
        "meta_description": null,
        "meta_keywords": null,
        "meta_title": null,
        "name": "Kategoria 1 poz",
        "path": "1/2/6",
        "path_in_store": null,
        "position": 3,
        "product_count": 2,
        "updated_at": "2020-02-02 16:58:24",
        "url_key": "kategoria-1-poz",
        "url_path": "kategoria-1-poz"
    },
    {
        "available_sort_by": [],
        "children_count": "5",
        "created_at": "2019-08-01 12:31:51",
        "default_sort_by": null,
        "description": "<p>test opis kategorii</p>",
        "display_mode": "PRODUCTS",
        "filter_price_range": null,
        "id": 3,
        "image": null,
        "include_in_menu": 1,
        "is_anchor": 0,
        "landing_page": null,
        "level": 2,
        "meta_description": null,
        "meta_keywords": null,
        "meta_title": null,
        "name": "Test",
        "path": "1/2/3",
        "path_in_store": null,
        "position": 1,
        "product_count": 14,
        "updated_at": "2020-08-05 10:28:23",
        "url_key": "test-seo",
        "url_path": "test-seo"
    },
    {
        "available_sort_by": [],
        "children_count": "1",
        "created_at": "2020-01-27 06:08:23",
        "default_sort_by": null,
        "description": null,
        "display_mode": "PRODUCTS",
        "filter_price_range": null,
        "id": 5,
        "image": null,
        "include_in_menu": 1,
        "is_anchor": 1,
        "landing_page": null,
        "level": 2,
        "meta_description": null,
        "meta_keywords": null,
        "meta_title": null,
        "name": "Test2",
        "path": "1/2/5",
        "path_in_store": null,
        "position": 2,
        "product_count": 1,
        "updated_at": "2020-03-17 10:00:46",
        "url_key": "test2",
        "url_path": "test2"
    },
]
const products = [
    {
        "canonical_url": "http://magento23.g4n.eu/test-simple-product.html",
        "description": {
            "html": ""
        },
        "id": 1,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Test simple product"
        },
        "name": "Test simple product",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Test simple product",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Test simple product"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Test simple product"
        },
        "type_id": "simple",
        "updated_at": "2020-09-28 09:24:55",
        "url_key": "test-simple-product",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/group-product.html",
        "description": {
            "html": ""
        },
        "id": 2,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Group product"
        },
        "name": "Group product",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 0,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Group product",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Group product"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Group product"
        },
        "type_id": "grouped",
        "updated_at": "2019-10-28 17:25:11",
        "url_key": "group-product",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/simple-product-2.html",
        "description": {
            "html": ""
        },
        "id": 3,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Simple product 2"
        },
        "name": "Simple product 2",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 150,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 150,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Simple product 2",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Simple product 2"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Simple product 2"
        },
        "type_id": "virtual",
        "updated_at": "2019-10-28 17:32:58",
        "url_key": "simple-product-2",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/simple-product-3.html",
        "description": {
            "html": ""
        },
        "id": 4,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Simple product 3"
        },
        "name": "Simple product 3",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 200,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 200,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Simple product 3",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Simple product 3"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Simple product 3"
        },
        "type_id": "simple",
        "updated_at": "2019-11-04 16:49:22",
        "url_key": "simple-product-3",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/bundle-product.html",
        "description": {
            "html": "<p>TESTTESTTEST</p>"
        },
        "id": 5,
        "image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/t/e/test-prd.jpg",
            "label": "Bundle product"
        },
        "name": "Bundle product",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Bundle product",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/t/e/test-prd.jpg",
            "label": "Bundle product"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/t/e/test-prd.jpg",
            "label": "Bundle product"
        },
        "type_id": "bundle",
        "updated_at": "2020-08-24 06:58:42",
        "url_key": "bundle-product",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/produkt-testowy-1.html",
        "description": {
            "html": ""
        },
        "id": 6,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Produkt testowy 1"
        },
        "name": "Produkt testowy 1",
        "new_from_date": "2019-11-04 00:00:00",
        "new_to_date": "2019-11-15 00:00:00",
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 25,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "12345",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Produkt testowy 1"
        },
        "special_from_date": "2019-11-03 00:00:00",
        "special_price": 25,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Produkt testowy 1"
        },
        "type_id": "simple",
        "updated_at": "2020-04-08 11:02:37",
        "url_key": "produkt-testowy-1",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/test2.html",
        "description": {
            "html": ""
        },
        "id": 7,
        "image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/l/a/lampy_wew.jpg",
            "label": "TEST2"
        },
        "name": "TEST2",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 0,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 0,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "TEST2",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/l/a/lampy_wew.jpg",
            "label": "TEST2"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "IN_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/l/a/lampy_wew.jpg",
            "label": "TEST2"
        },
        "type_id": "configurable",
        "updated_at": "2019-11-04 17:21:29",
        "url_key": "test2",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/grouped-product.html",
        "description": {
            "html": ""
        },
        "id": 26,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Grouped product"
        },
        "name": "Grouped product",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 0,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Grouped product",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Grouped product"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "IN_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Grouped product"
        },
        "type_id": "grouped",
        "updated_at": "2019-11-04 17:30:32",
        "url_key": "grouped-product",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/bundle-product-2.html",
        "description": {
            "html": ""
        },
        "id": 28,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "bundle product 2"
        },
        "name": "bundle product 2",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 0,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 0,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "bundle product-1",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "bundle product 2"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "IN_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "bundle product 2"
        },
        "type_id": "bundle",
        "updated_at": "2019-11-04 17:48:37",
        "url_key": "bundle-product-2",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/download-me.html",
        "description": {
            "html": ""
        },
        "id": 29,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Download ME"
        },
        "name": "Download ME",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 666,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 666,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Download ME",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Download ME"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Download ME"
        },
        "type_id": "downloadable",
        "updated_at": "2019-11-04 18:04:20",
        "url_key": "download-me",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/produkt-zestaw.html",
        "description": {
            "html": ""
        },
        "id": 30,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Produkt zestaw"
        },
        "name": "Produkt zestaw",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 0,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "set123456",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Produkt zestaw"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "IN_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Produkt zestaw"
        },
        "type_id": "grouped",
        "updated_at": "2020-02-20 12:18:19",
        "url_key": "produkt-zestaw",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/autotest-simple.html",
        "description": {
            "html": ""
        },
        "id": 31,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Autotest Simple"
        },
        "name": "Autotest Simple",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 100,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Autotest Simple",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Autotest Simple"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Autotest Simple"
        },
        "type_id": "simple",
        "updated_at": "2020-03-20 12:17:19",
        "url_key": "autotest-simple",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/eds-2005-el.html",
        "description": {
            "html": "<p>Przemysłowe przełączniki Ethernet serii EDS-2005-EL to udoskonalone wersje modeli serii EDS-205A. Posiadają 5 miedzianych portów 10 / 100M, które idealnie nadają się do zastosowań wymagających nieskomplikowanych połączeń Ethernet w instalacjach przemysłowych. Dodatkowo, aby zapewnić większą wszechstronność w instalacjach z różnych branż, seria EDS-2005-EL umożliwia włączanie obsługi kolejkowania ruchu (QoS) oraz ochronę przed burzą broadcastową (BSP) za pomocą przełączników DIP na obudowie. EDS-2005-EL posiada wytrzymałą metalową obudowę dla zapewnienia większej odporności w środowisku przemysłowym.</p>\r\n<p>Seria EDS-2005-EL posiada pojedyncze wejście zasilania 12/24/48 VDC, montaż na szynie DIN i wysoką wytrzymałość EMI / EMC. Oprócz niewielkich rozmiarów urządzenia tej rodziny mogą pracować w zakresie temperatur od -10 do 60 ° C, a modele rozszerzone -40 do 75 ° C (oznaczone -T).</p>"
        },
        "id": 32,
        "image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/m/o/moxa-eds-2005-el-series-image.jpg",
            "label": "EDS-2005-EL"
        },
        "name": "EDS-2005-EL",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 315,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 315,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": "<p>5-portowy, kompaktowy switch niezarządzalny w metalowej obudowie</p>\r\n<ul>\r\n<li>porty 10/100BaseT(x) (złącza RJ45)</li>\r\n<li>bardzo małe wymiary (kompaktowa obudowa)</li>\r\n<li>wbudowane kolejkowanie QoS</li>\r\n<li>obudowa o stopniu ochrony IP40</li>\r\n<li>temperatura pracy -40 to 75°C (dotyczy wersji -T)</li>\r\n</ul>"
        },
        "sku": "EDS-2005-EL",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/m/o/moxa-eds-2005-el-series-image.jpg",
            "label": "EDS-2005-EL"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": "/m/o/moxa-eds-2005-el-series-image.jpg",
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/m/o/moxa-eds-2005-el-series-image.jpg",
            "label": "EDS-2005-EL"
        },
        "type_id": "simple",
        "updated_at": "2020-04-02 07:24:57",
        "url_key": "eds-2005-el",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/prosty-testowy.html",
        "description": {
            "html": ""
        },
        "id": 33,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "Prosty testowy"
        },
        "name": "Prosty testowy",
        "new_from_date": null,
        "new_to_date": null,
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 150,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 150,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "Prosty testowy",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "Prosty testowy"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "IN_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "Prosty testowy"
        },
        "type_id": "simple",
        "updated_at": "2020-09-28 10:16:34",
        "url_key": "prosty-testowy",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/new-bundle.html",
        "description": {
            "html": ""
        },
        "id": 34,
        "image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/c/o/code.jpg",
            "label": "New Bundle"
        },
        "name": "New Bundle",
        "new_from_date": "2020-10-04 00:00:00",
        "new_to_date": "2026-10-29 00:00:00",
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 2300,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 2300,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "NewBundle",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/c/o/code.jpg",
            "label": "New Bundle"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/media/catalog/product/cache/1dc922a225afcace74d03390ace43e2e/c/o/code.jpg",
            "label": "New Bundle"
        },
        "type_id": "bundle",
        "updated_at": "2020-10-27 11:05:25",
        "url_key": "new-bundle",
        "url_path": null
    },
    {
        "canonical_url": "http://magento23.g4n.eu/test-product.html",
        "description": {
            "html": ""
        },
        "id": 35,
        "image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/image.jpg",
            "label": "test product"
        },
        "name": "test product",
        "new_from_date": "2021-02-25 00:00:00",
        "new_to_date": "2025-02-28 00:00:00",
        "price": {
            "maximalPrice": {
                "adjustments": []
            },
            "minimalPrice": {
                "amount": {
                    "value": 20,
                    "currency": "PLN"
                }
            },
            "regularPrice": {
                "amount": {
                    "value": 20,
                    "currency": "PLN"
                },
                "adjustments": []
            }
        },
        "short_description": {
            "html": ""
        },
        "sku": "test product",
        "small_image": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/small_image.jpg",
            "label": "test product"
        },
        "special_from_date": null,
        "special_price": null,
        "special_to_date": null,
        "stock_status": "OUT_OF_STOCK",
        "swatch_image": null,
        "thumbnail": {
            "url": "http://magento23.g4n.eu/pub/static/version1563518494/graphql/_view/pl_PL/Magento_Catalog/images/product/placeholder/thumbnail.jpg",
            "label": "test product"
        },
        "type_id": "simple",
        "updated_at": "2021-03-01 20:43:01",
        "url_key": "test-product",
        "url_path": null
    }
];

export { config, products, categories }