import { print } from "graphql";

export default (query, options) => {
    const queryParsed = print(query).trim();
    const queryParsedEncoded = encodeURIComponent(queryParsed);
    const optionsParsed =
        options && Object.keys(options)
            ? Object.keys(options).reduce((result, key) => {
                const valueString = JSON.stringify(options[key]);
                const valueStringEncoded = encodeURIComponent(valueString);
                return `${result}&${key}=${valueStringEncoded}`;
            }, "")
            : "";

    return `/graphql?query=${queryParsedEncoded}${optionsParsed}`;
};