import React, {useEffect, useState} from "react";
import {gql, useQuery} from "@apollo/client";
import {BrowserRouter, Link} from "react-router-dom";
import Routing from "../Routing";
import { connect } from "react-redux"
import {bindActionCreators} from "redux";
import { createCartAsyncAction, getCartDetailsAsyncAction } from "../../actions/cart";
import Header from "../Header";
import Footer from "../Footer";
import "./app.css"
import useQueryGet from "../../useQueryGet";

const storeConfigQuery = gql`
    query storeConfig{
        storeConfig{
            absolute_footer
            base_currency_code
            base_link_url
            base_media_url
            base_static_url
            base_url
            cms_home_page
            cms_no_cookies
            cms_no_route
            code
            copyright
            default_description
            default_display_currency_code
            default_keywords
            default_title
            demonotice
            front
            head_includes
            head_shortcut_icon
            header_logo_src
            id
            locale
            logo_alt
            logo_height
            logo_width
            no_route
            secure_base_link_url
            secure_base_media_url
            secure_base_static_url
            secure_base_url
            show_cms_breadcrumbs
            timezone
            title_prefix
            title_suffix
            website_id
            weight_unit
            welcome
        }
    }
`;

export const AppContext = React.createContext();

const App = ({ creatCartAction, getCartDetails }) => {
    useEffect(() => {
        const fetchData = async () => {
            await creatCartAction();
            getCartDetails();
        }

        fetchData();
    }, [creatCartAction, getCartDetails]);

    const { loading, error, data } = useQueryGet({ query: storeConfigQuery} )

    switch (true) {
        case !!loading: {
            return <div>Ladowanie...</div>
        }
        case !!error: {
            console.error(error);
            return <div>Jakis blad wystapil!</div>
        }
        default: {
            return (
                <AppContext.Provider value={data ? data.storeConfig : {}}>
                    <BrowserRouter>
                        <div className="container">
                            <Header />
                            <Routing />
                            <Footer />
                        </div>
                    </BrowserRouter>
                </AppContext.Provider>
            )
        }
    }
}

const putActionsToProps = (dispatch) => {
    return {
        creatCartAction: bindActionCreators(createCartAsyncAction, dispatch),
        getCartDetails: bindActionCreators(getCartDetailsAsyncAction, dispatch),
    }
}

export default connect(null, putActionsToProps)(App);