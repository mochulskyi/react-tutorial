import React, { useState } from "react";
import appActions from "../../actions/app";
import { connect } from "react-redux"
import { bindActionCreators } from "redux"

const ProductList = ({ products, name, children, isDrawerOpened, openDrawer, closeDrawer }) => {
    const productsSliced = products.slice(0, 6);
    const [items, setItems] = useState(productsSliced);
    const clickHandler = () => {
        const currentCount = (items.length / 6) + 1;
        const nextItems = products.slice(0, 6 * currentCount);
        setItems(nextItems);
    }
    const toggleDrawerHandler = () => {
        if (isDrawerOpened) {
            closeDrawer()
        } else {
            openDrawer()
        }
    }

    return <div>
        <h1>PRODUCT PAGE</h1>
        {items.map(item => {
            return (
                <div key={item.id}>
                    <h2>{item.name}</h2>
                </div>
            )
        })}
        {products.length === items.length ? null : (
            <button type="button" onClick={clickHandler}>{name}</button>
        )}
        {children}
        <button type="button" onClick={toggleDrawerHandler}>Toggle drawer</button>
    </div>;
}

const putActionsToProps = (dispatch) => {
    return {
        openDrawer: bindActionCreators(appActions.drawerOpen, dispatch),
        closeDrawer: bindActionCreators(appActions.drawerClose, dispatch)
    }
}

const putStateToProps = (state) => {
    return {
        isDrawerOpened: state.app.isDrawerOpened
    }
}


export default connect(putStateToProps, putActionsToProps)(ProductList);