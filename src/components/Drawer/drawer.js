import React from "react";
import { connect } from "react-redux"

const Drawer = ({ isDrawerOpened }) => {
    return isDrawerOpened ? <div>
        <h1>DRAWER</h1>
    </div> : null;
}

const putStateToProps = (state) => {
    return {
        isDrawerOpened: state.app.isDrawerOpened
    }
}

export default connect(putStateToProps)(Drawer);