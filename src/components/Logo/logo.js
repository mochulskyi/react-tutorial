import React from "react";
import { Link } from "react-router-dom";
import { AppContext } from "../App"
import "./logo.css"

const LogoWrapper = () => {
    return <AppContext.Consumer>
        {value => <Logo mediaSrc={value ? value.base_media_url : ""} logoSrc={value ? value.header_logo_src : ""} />}
    </AppContext.Consumer>
}

const Logo = ({ mediaSrc, logoSrc }) => {
    const src = `${mediaSrc}logo/${logoSrc}`;
    return (
        <Link className="logo" to="/">
            <img src={src} alt="LOGOTYP" />
        </Link>
    );
}

export default LogoWrapper;