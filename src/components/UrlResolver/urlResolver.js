import React from "react";
import { gql } from '@apollo/client';
import { useLocation, Redirect } from "react-router-dom";
import Category from "../Category";
import useQueryGet from "../../useQueryGet";

const UrlResolver = () => {
    const { pathname } = useLocation();
    const urlResolverQuery = gql`
        query{
            urlResolver(url: "${pathname}"){
                canonical_url
                id
                relative_url
                type
            }
        }
    `;
    const { loading, error, data } = useQueryGet({ query: urlResolverQuery });

    switch (true) {
        case !!loading: {
            return <div>Ladowanie urlResolver</div>
        }
        case !!error: {
            console.error(error);
            return <div>Blad urlResolver</div>
        }
        default: {
            switch (data && data.urlResolver && data.urlResolver.type) {
                case "CMS_PAGE": {
                    return <div>CMS_PAGE</div>
                }
                case "PRODUCT": {
                    return <div>PRODUCT</div>
                }
                case "CATEGORY": {
                    return <Category id={data.urlResolver.id} />
                }
                case "REDIRECT": {
                    return <Redirect to={data.relative_url} />
                }
                default: {
                    return <div>404 Page</div>
                }
            }
        }
    }

    return <div>UrlResolver</div>;
}

export default UrlResolver;