import React from "react";
import {Link} from "react-router-dom";
import Logo from "../Logo";
import "./header.css"
import Minicart from "../Minicart";

const Header = () => {
    return <header className="header">
        <div className="header__logo">
            <Logo />
        </div>
        <nav className="header__nav">
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/test-seohtml">Products Category</Link>
                </li>
            </ul>
        </nav>
        <div className="header__minicart">
            <Minicart />
        </div>
    </header>;
}

export default Header;