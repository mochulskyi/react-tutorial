import React from "react";
import {gql} from "@apollo/client";
import "./category.css";
import Product from "../Product";
import useQueryGet from "../../useQueryGet";

const Category = ({ id }) => {
    const query = gql`
        query {
            category(id: ${id}) {
                available_sort_by
                breadcrumbs {
                    category_id
                    category_level
                    category_name
                    category_url_key
                }
                children_count
                created_at
                default_sort_by
                description
                display_mode
                filter_price_range
                id
                image
                include_in_menu
                is_anchor
                landing_page
                level
                meta_description
                meta_keywords
                meta_title
                name
                path
                path_in_store
                position
                product_count
                updated_at
                url_key
                url_path
                products(pageSize: 20, currentPage: 1) {
                    total_count
                    items {
                        attribute_set_id
                        canonical_url
                        color
                        country_of_manufacture
                        created_at
                        gift_message_available
                        id
                        kolor
                        liczba_portow_ethernet_100_mbp
                        manufacturer
                        meta_description
                        meta_keyword
                        meta_title
                        name
                        new_from_date
                        new_to_date
                        options_container
                        price {
                            maximalPrice {
                                adjustments {
                                    amount {
                                        currency
                                        value
                                    }
                                    code
                                }
                                amount {
                                    currency
                                    value
                                }
                            }
                            minimalPrice {
                                adjustments {
                                    amount {
                                        currency
                                        value
                                    }
                                    code
                                }
                                amount {
                                    currency
                                    value
                                }
                            }
                            regularPrice {
                                adjustments {
                                    amount {
                                        currency
                                        value
                                    }
                                    code
                                }
                                amount {
                                    currency
                                    value
                                }
                            }
                        }
                        rozmiar
                        sku
                        special_from_date
                        special_price
                        special_to_date
                        stock_status
                        swatch_image
                        thumbnail {
                            label
                            url
                        }
                        tier_price
                        type_id
                        updated_at
                        url_key
                        url_path
                    }
                }
            }
        }
    `;
    const { loading, error, data } = useQueryGet({ query });
    if (loading || error) {
        return null;
    }

    const { category } = data;

    return <div className="category">
        <h1>{category.name}</h1>
        <div dangerouslySetInnerHTML={{__html: category.description}} />
        <ul className="products">
            {category.products.items.map(product => {
                return (
                    <li key={product.id} className="products__item">
                        <Product product={product} />
                    </li>
                )
            })}
        </ul>
    </div>
}

// class Category extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             isOpen: true
//         }
//         this.timeOutId = null;
//     }
//
//     render() {
//         const { category } = this.props;
//         return <div>
//             {category.name ? (
//                 <h1>{category.name}</h1>
//             ) : null}
//             {category.description ? (
//                 <>
//                     {this.state.isOpen ? (
//                         <p dangerouslySetInnerHTML={{__html: category.description}}/>
//                     ) : null}
//                     <button type="button" onClick={this.handleClick}>Toggle description</button>
//                 </>
//             ) : null}
//         </div>
//     }
//
//     handleClick = () => {
//         this.setState({
//             isOpen: !this.state.isOpen
//         });
//     }
// }

export default Category;