import React from "react";
import {Route, Switch} from "react-router-dom";
import ProductList from "../ProductList/productList";
import {categories, products} from "../../fixtures";
import Info from "../Info";
import Drawer from "../Drawer/drawer";
import CategoryList from "../CategoryList";
import UrlResolver from "../UrlResolver";

const Routing = () => {
    return (
        <Switch>
            <Route path="/home">
                <CategoryList categories={categories}/>
            </Route>
            <Route path="/products">
                <ProductList name={""} products={products}>
                    <Info/>
                </ProductList>
                <Drawer/>
            </Route>
            <Route>
                <UrlResolver />
            </Route>
        </Switch>
    )
}

export default Routing;