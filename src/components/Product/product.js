import React from "react";
import { Link } from "react-router-dom";
import {bindActionCreators} from "redux";
import { addProductToCart } from "../../actions/cart";
import {connect} from "react-redux";

const Product = ({ product, addToCart }) => {
    const productUrl = `/${product.url_key}.html`;
    const clickHandler = () => {
        addToCart({
            sku: product.sku,
            qty: 1,
            name: product.name,
            product_type: product.type_id
        });
    }
    return (
        <article>
            <div>
                <Link to={productUrl}>
                    <img src={product.thumbnail.url} alt={product.thumbnail.label} />
                </Link>
            </div>
            <h2><Link to={productUrl}>{product.name}</Link></h2>
            {product.stock_status === "OUT_OF_STOCK" ?
                (
                    <p>Product niedostepny</p>
                ) : (
                <button type="button" onClick={clickHandler}>Dodaj do koszyka</button>
            )}
        </article>
    )
}

const putActionsToProps = (dispatch) => {
    return {
        addToCart: bindActionCreators(addProductToCart, dispatch),
    }
}

export default connect(null, putActionsToProps)(Product)