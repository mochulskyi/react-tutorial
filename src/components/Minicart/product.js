import React from "react";
import {Link} from "react-router-dom";

const Product = ({ price, name, urlKey, image }) => {
    const url = `/${urlKey}.html`;
    return <article>
        <Link to={url}>
            <img src={image} alt={name} />
        </Link>
        <h2><Link to={url}>{name}</Link></h2>
        <strong>{price}</strong>
    </article>
}

export default Product;