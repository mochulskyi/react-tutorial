import React from "react";
import {gql} from "@apollo/client";
import Product from "./product";
import useQueryGet from "../../useQueryGet";


const Wrapper = ({ items }) => {
    return items.length ? <Dropdown items={items} /> : "Nie sa dodane produkty do koszyka";
}

const Dropdown = ({ items }) => {
    const skus = items.map(item => item.sku);
    const getProductsInfo = gql`
        query Products {
            products(filter: { sku: {
                finset: ["${skus.toString()}"]
            } }) {
                items {
                    sku
                    url_key
                    small_image {
                        url
                        label
                    }
                }
            }
        }
    `;
    const { loading, error, data } = useQueryGet({query: getProductsInfo});

    switch (true) {
        case !!loading: {
            return "Ladowanie";
        }
        case !!error: {
            console.error(error)
            return "Blad!!!"
        }
        default: {
            const itemsProcessed = items.map(item => {
                const currentItemData = data.products.items.find(productItem => productItem.sku === item.sku);
                return currentItemData ? {
                    ...currentItemData,
                    ...item
                } : item
            });

            return (
                <ul>
                    {itemsProcessed.map(item => {
                        return (
                            <li key={item.sku}>
                                <Product price={item.price} name={item.name} urlKey={item.url_key} image={item.small_image.url} />
                            </li>
                        )
                    })}
                </ul>
            )
        }
    }
}

export default Wrapper;