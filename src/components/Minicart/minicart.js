import React, {useState} from "react";
import "./minicart.css"
import {connect} from "react-redux";
import Dropdown from "./dropdown";

const Minicart = ({ count , total = 0, qty, items }) => {
    const [isOpen, toggleOpen] = useState(false);
    return <div className="minicart">
        <button className="minicart__trigger" type="button" onClick={() => toggleOpen(!isOpen)}>
            <span className="minicart__icon" />
            <span className="minicart__count">QTY: {qty}</span>
            <span className="minicart__count">COUNT: {count}</span>
            <span className="minicart__total">TOTAL: {total}</span>
        </button>
        {isOpen ? <div className="minicart__dropdown">
            <Dropdown items={items} />
        </div> : null}
    </div>;
}

const putStateToProps = ({ cart }) => {
    return {
        items: cart.items,
        count: cart.count,
        qty: cart.qty
    }
}

export default connect(putStateToProps)(Minicart);