import React, { useState } from "react";
import Category from "../Category";

const CategoryList = (props) => {
    const { categories } = props;
    const categoriesSliced = categories.slice(0, 1)
    const [isShown, toggleShown] = useState(true);

    return (
        <>
            {isShown ? (
                <ul>
                    {categoriesSliced.map((item, index) => {
                        return index === 0 ?(
                            <li key={item.id}>
                                <Category category={item} />
                            </li>
                        ) : (
                            <li key={item.id}>
                                <Category category={item}/>
                            </li>
                        )
                    })}
                </ul>
            ) : null}
            <button type="button" onClick={() => toggleShown(!isShown)}>toggleShown</button>
        </>
    )
}

export default CategoryList;