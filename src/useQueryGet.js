import {useEffect, useState} from "react";
import convertQueryToGet from "./convertQueryToGet";


const options = {
    method: "GET",
    credentials: "include",
    headers: new Headers({
        "Content-Type": "application/json",
    }),
};

export default ({ query, variables }) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [data, setData] = useState(null);

    useEffect(() => {
        const uri = convertQueryToGet(query, variables);
        fetch(uri, options)
            .then((response) => response.json())
            .then(response => {
                setData(response.data);
                setLoading(false)
            }).catch(error => {
            setError(error);
            setLoading(false);
        })
    }, [query]);

    return {loading, error, data}
}