const { createProxyMiddleware } = require('http-proxy-middleware');

const options = {
    target: 'http://magento23.g4n.eu',
    changeOrigin: true,
};

const myProxy = createProxyMiddleware(['/graphql', '/rest'], options);

module.exports = function(app) {
    app.use(myProxy);
};