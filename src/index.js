import React from "react"
import ReactDOM from "react-dom"
import {Provider} from "react-redux"
import {store} from "./store"
import {ApolloClient, InMemoryCache} from '@apollo/client';
import {ApolloProvider} from '@apollo/client/react';
import App from "./components/App";

const client = new ApolloClient({
    uri: "/graphql",
    cache: new InMemoryCache()
});

ReactDOM.render(
    <ApolloProvider client={client}>
        <Provider store={store}>
            <App />
        </Provider>
    </ApolloProvider>, document.getElementById("root"));

window.addEventListener("load", function () {
    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('./sw.js')
            .then((reg) => {
               console.log("serviceWorker registered", reg)
            }).catch((error) => {
            console.log('Registration failed with ' + error);
        });
    }
})