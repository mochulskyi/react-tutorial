export default {
    drawerOpen: "APP_DRAWER_OPEN",
    drawerClose: "APP_DRAWER_CLOSE",
    overlayShow: "APP_OVERLAY_SHOW",
    overlayHide: "APP_OVERLAY_HIDE"
}