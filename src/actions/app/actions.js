import types from "./types";
export default {
    drawerOpen: () => {
        return {
            type: types.drawerOpen,
            payload: true
        }
    },
    drawerClose: () => {
        return {
            type: types.drawerClose,
            payload: false
        }
    },
    overlayShow: () => {
        return {
            type: types.overlayShow,
            payload: true
        }
    },
    overlayHide: () => {
        return {
            type: types.overlayHide,
            payload: false
        }
    }
}