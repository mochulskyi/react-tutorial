import actions from "./actions"

export const createCartAsyncAction = () => {
    return async (dispatch) => {
        const cartId = localStorage.getItem('cartId');

        if (!cartId) {
            const response = await fetch("/rest/V1/guest-carts", {
                method: 'POST'
            });
            const responseParsed = await response.json();
            localStorage.setItem('cartId', responseParsed);
            dispatch(actions.createCart(responseParsed));
        } else {
            dispatch(actions.createCart(cartId));
        }
    };
}

export const getCartDetailsAsyncAction = () => {
    return async (dispatch, getState) => {
        dispatch(actions.getCartDetailsRequest());
        const state = getState();
        const cartId = state.cart.cartId;
        const response = await fetch(`/rest/V1/guest-carts/${cartId}`, {
            method: 'GET'
        });
        const responseParsed = await response.json();
        dispatch(actions.getCartDetailsReceive(responseParsed));
    }
}

export const addProductToCart = (payload) => {
    return async (dispatch, getState) => {
        const state = getState();
        const cartId = state.cart.cartId;
        const response = await fetch(`/rest/V1/guest-carts/${cartId}/items`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                cartItem: {
                    // item_id: "",
                    sku: payload.sku,
                    qty: payload.qty,
                    name: payload.name,
                    quote_id: cartId,
                    product_type: payload.product_type
                }
            })
        });
        const responseParsed = await response.json();
        console.log("addProductToCart", responseParsed);
        dispatch(getCartDetailsAsyncAction())
    }
}