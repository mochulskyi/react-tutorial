export default {
    createCart: "CART_CREATE_CART",
    getCartDetailsRequest: "CART_GET_DETAILS_REQUEST",
    getCartDetailsReceive: "CART_GET_DETAILS_RECEIVE",
}