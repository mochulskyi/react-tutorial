import types from "./types";
export default {
    createCart: (payload) => {
        return {
            type: types.createCart,
            payload
        }
    },
    getCartDetailsRequest: () => {
        return {
            type: types.getCartDetailsRequest,
        }
    },
    getCartDetailsReceive: (payload) => {
        return {
            type: types.getCartDetailsReceive,
            payload
        }
    }
}