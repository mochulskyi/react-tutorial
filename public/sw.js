const staticCacheName = "static-v6";

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(staticCacheName).then((cache) => {
            return cache.addAll([
                './index.html',
            ]);
        })
    );
});

self.addEventListener("activate", (event) => {
    event.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(
                keyList
                    .filter(key => key !== staticCacheName)
                    .map(key => caches.delete(key))
            );
        })
    );
})

self.addEventListener("fetch", function (event) {
    event.respondWith(
        caches.match(event.request)
            .then(function (response) {
                if (response !== undefined) {
                    return response;
                } else {
                    return fetch(event.request)
                        .then(function (response) {
                            let responseClone = response.clone();
                            caches.open(staticCacheName).then(function (cache) {
                                cache.put(event.request, responseClone);
                            });
                            return response;
                        })
                }
            })
    );
})